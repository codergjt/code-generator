package com.ddd.codegeneration.entity.entity;

/**
 * @author 官江涛
 * @version 1.0
 * @Title: 字段
 * @date 2018/7/22/22:11
 */
public class Column {

    /**
     * 字段名称
     */
    private  String columName;
    /**
     * 字段类型
     */
    private  String columType;
    /**
     * 字段中文名称
     */
    private  String columChineseName;
    /**
     * 约束关系
     */
    private  String constraintRelation;
    /**
     * 组合关系
     */
    private String  syntheticRelation;
    /**
     * 备注
     */
    private String  remarks;

    public Column(){
        super();
    }

    public Column(String columName, String columType, String columChineseName, String constraintRelation, String syntheticRelation, String remarks) {
        this.columName = columName;
        this.columType = columType;
        this.columChineseName = columChineseName;
        this.constraintRelation = constraintRelation;
        this.syntheticRelation = syntheticRelation;
        this.remarks = remarks;
    }

    public String getColumName() {
        return columName;
    }

    public void setColumName(String columName) {
        this.columName = columName;
    }

    public String getColumType() {
        return columType;
    }

    public void setColumType(String columType) {
        this.columType = columType;
    }

    public String getColumChineseName() {
        return columChineseName;
    }

    public void setColumChineseName(String columChineseName) {
        this.columChineseName = columChineseName;
    }

    public String getConstraintRelation() {
        return constraintRelation;
    }

    public void setConstraintRelation(String constraintRelation) {
        this.constraintRelation = constraintRelation;
    }

    public String getSyntheticRelation() {
        return syntheticRelation;
    }

    public void setSyntheticRelation(String syntheticRelation) {
        this.syntheticRelation = syntheticRelation;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}
