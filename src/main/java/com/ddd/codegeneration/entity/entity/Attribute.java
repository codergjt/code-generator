package com.ddd.codegeneration.entity.entity;

/**
 * @author 官江涛
 * @version 1.0
 * @Title:  实体对应的值
 * @date 2018/7/23/13:29
 */
public class Attribute {

    private String name;

    private String type;

    public Attribute(){}

    public Attribute(String name, String type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
