package com.ddd.codegeneration.entity.entity;

import java.util.List;

/**
 * @author 官江涛
 * @version 1.0
 * @Title: 实体参数
 * @date 2018/7/22/21:47
 */
public class EntityParameter extends BaseEntity {

    /**
     * 实体名称
     */
    private String entityName;

    /**
     * 实体中文名称
     */
    private String entityChineseName;

    /**
     * 设置模块
     */
    private String importModelName;

    /**
     * 字段
     */
    private List<Column> columns;

    public EntityParameter(){}

    public EntityParameter(String entityName, String entityChineseName, String importModelName, List<Column> columns) {
        this.entityName = entityName;
        this.entityChineseName = entityChineseName;
        this.importModelName = importModelName;
        this.columns = columns;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public String getEntityChineseName() {
        return entityChineseName;
    }

    public void setEntityChineseName(String entityChineseName) {
        this.entityChineseName = entityChineseName;
    }

    public String getImportModelName() {
        return importModelName;
    }

    public void setImportModelName(String importModelName) {
        this.importModelName = importModelName;
    }

    public List<Column> getColumns() {
        return columns;
    }

    public void setColumns(List<Column> columns) {
        this.columns = columns;
    }

    /**
     * 首字母大写
     * 主要用于生成get/set方法
     * @param words 传入需要转换的字符串
     * @return 返回结果
     */
    public String fristWordToUpperCase(String words) {
        try {
            if (words != null) {
                String temp = words.substring(0, 1);
                return temp.toUpperCase() + words.substring(1, words.length());
            }
        } catch (Exception e) {
            System.err.println("发生错误");
            e.printStackTrace();
        }
        return null;
    }
}
