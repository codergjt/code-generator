package com.ddd.codegeneration.service;

import java.util.Map;

/**
 * @author 官江涛
 * @version 1.0
 * @Title: 基础代码生成
 * @date 2018/7/24/21:54
 */
public interface GeneratorBaseCodeService {

    int generateApi(Map<String, Object> root, String servicePath);
}
