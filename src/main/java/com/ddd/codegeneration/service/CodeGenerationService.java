package com.ddd.codegeneration.service;

import com.ddd.codegeneration.entity.entity.EntityParameter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author 官江涛
 * @version 1.0
 * @Title:  代码生成Service
 * @date 2018/7/22/22:23
 */
@Component
@Transactional
public interface CodeGenerationService {
    /**
     * 生成java代码
     * @param entityParameter
     * @return
     */
    String generateJavaCode(EntityParameter entityParameter);

    /**
     * 根据实体名称生成VUE代码
     * @param entityUrl   实体的类全名
     * @return  true or false
     */
    boolean generateBaseCode(String entityUrl);
}
