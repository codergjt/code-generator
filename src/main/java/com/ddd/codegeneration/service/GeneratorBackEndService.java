package com.ddd.codegeneration.service;

import java.util.Map;

/**
 * @author 官江涛
 * @version 1.0
 * @Title:  后台代码生成器
 * @date 2018/7/23/10:21
 */
public interface GeneratorBackEndService {

    String generateEntity(Map<String, Object> attrs, String entityPath);

    int generateService(Map<String, Object> root, String servicePath);

    int generateServiceImpl(Map<String, Object> root, String controllerPath);

    int generateController(Map<String, Object> root, String serviceImplPath);

    int generateJavaMapper(Map<String, Object> root, String mapperPath);

    int generateXMLMapper(Map<String, Object> root, String xmlPath);
}
