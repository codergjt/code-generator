package com.ddd.codegeneration.controller;

import com.ddd.codegeneration.entity.entity.EntityParameter;
import com.ddd.codegeneration.service.CodeGenerationService;
import com.ddd.codegeneration.utils.JSONResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author 官江涛
 * @version 1.0
 * @Title:  代码生成接口，用于接受前端传输过来的数据
 * @date 2018/7/22/21:26
 */
@RequestMapping(value = "codeGeneration")
@CrossOrigin
@RestController
public class CodeGenerationController {

    @Autowired
    private CodeGenerationService codeGenerationService;

//    public CodeGenerationController() {
//        codeGenerationService = new CodeGenerationServiceImpl();
//    }

    /**
     * java代码生成接口
     * @param entityParameter  前台传过来的实体参数
     * @return 返回生成成功的实体路径
     */
    @RequestMapping(method = RequestMethod.POST, value = "/createdJavaCode")
    public JSONResult createdJavaCode(@Valid @RequestBody EntityParameter entityParameter) {
        JSONResult jsonResult = new JSONResult<>();
        Object message = "entityUrl";
        try {
            String entityURL = codeGenerationService.generateJavaCode(entityParameter);
            if(entityURL != null){
                jsonResult.setMessage("生成成功");
                jsonResult.setData(entityURL);
            } else {
                jsonResult.setMessage("生成失败");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return jsonResult;
    }

    /**
     * 前端代码生成
     * @param entityURL   实体的类路径
     * @return     成功或者失败
     */
    @RequestMapping(method = RequestMethod.POST, value = "/createdWebCode")
    public JSONResult createdWebCode(@Valid @RequestBody String entityURL){
        JSONResult jsonResult = new JSONResult<>();

        boolean isOk = codeGenerationService.generateBaseCode(entityURL);
        if(isOk){
            jsonResult.setMessage("生成成功");
        }else {
            jsonResult.setMessage("生成失败");
        }
        return jsonResult;
    }
}
