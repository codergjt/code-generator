package com.ddd.codegeneration.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author 官江涛
 * @version 1.0
 * @Title:   工具类
 * @date 2018/7/24/21:47
 */
public class Utils {

    /**
     * 获取类类型信息
     * @param entityUrl
     * @return
     */
    public static Class getClazz(String entityUrl){
        Class clazz = null;
        try {
            // 获取类类型信息
            clazz = Class.forName(entityUrl);
        } catch (ClassNotFoundException e) {
            System.out.println("找不到指定类，请检查路径是否正确");
            e.printStackTrace();
        }
        return clazz;
    }

    /**
     * 格式化时间<br/>
     * 带时间分秒格式
     * @param time  时间
     * @return   返回结果：2017年9月15日 13:17:08:355
     */
    public static String typeOne(Date time) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy年MMM月dd日 HH:mm:ss:SSS");
        String formatStr = formatter.format(time);
        return formatStr;
    }

    /**
     * 格式化时间<br/>
     * 带时间分秒格式
     * @param time  时间
     * @return 返回结果：2017-09-15 13:18:44:672
     */
    public static String typeTwo(Date time){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
        String formatStr =formatter.format(time);
        return   formatStr;
    }

    /**
     * 格式化时间<br/>
     * 不带带时间分秒格式
     * @param time 时间
     * @return 返回结果：2017-09-15
     */
    public static String typeThree(Date time){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String formatStr =formatter.format(time);
        System.out.println(formatStr);
        return  formatStr;
    }

    /**
     * 格式化时间<br/>
     * @param time  时间
     * @return   返回结果：2017年9月15日
     */
    public static String typeFour(Date time) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy年MMM月dd日");
        String formatStr = formatter.format(time);
        return formatStr;
    }
}
