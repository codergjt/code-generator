package com.ddd.codegeneration.utils;

import freemarker.template.Configuration;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.HashMap;

/**
 * @author 官江涛
 * @version 1.0
 * @Title: 自定义yaml文件解析器
 * @date 2018/7/24/9:48
 */
public class FreeMarkerConfigUtils {

    /**
     *  自定义yaml文件解析器
     * @param configFilePath  文件名称
     * @return yaml文件中的数据，为一个对象。
     */
    public static HashMap getConfig(String configFilePath){
        Yaml yaml = new Yaml();
        HashMap<String,String> config = new HashMap<>();
        final String fileName = configFilePath;
        System.out.println(fileName);
        URL url = Configuration.class.getClassLoader().getResource(fileName);
        File conf = new File(url.getFile());
        try {
            config =(HashMap)yaml.load(new FileInputStream(conf));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return config;
    }

}
