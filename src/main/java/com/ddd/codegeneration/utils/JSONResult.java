package com.ddd.codegeneration.utils;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author 官江涛
 * @version 1.0
 * @Title:
 * @date 2018/7/22/21:35
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JSONResult<T> {
    public JSONResult() {
    }

    public JSONResult(String errCode, Object message) {
        this.errCode = errCode;
        this.message = message;
    }

    public JSONResult(Integer page, Integer pageSize, Integer totalCount, T data) {
        this.page = page;
        this.pageSize = pageSize;
        this.totalCount = totalCount;
        this.data = data;
    }

    public JSONResult(T data) {
        this.data = data;
    }

    /**
     * 错误代码
     */
    private String errCode;

    /**
     * 消息
     */
    private Object message;

    /**
     * 当前页
     */
    private Integer page;

    /**
     * 分页大小
     */
    private Integer pageSize;

    /**
     * 总记录数
     */
    private Integer totalCount;

    /**
     * 返回数据
     */
    private T data;

    /**
     * 返回 错误代码
     */
    public String getErrCode() {
        return errCode;
    }

    /**
     * 设置 错误代码
     */
    public void setErrCode(String errCode) {
        this.errCode = errCode;
    }

    /**
     * 返回 消息
     */
    public Object getMessage() {
        return message;
    }

    /**
     * 设置 消息
     */
    public void setMessage(Object message) {
        this.message = message;
    }

    /**
     * 设置 消息
     */
    public void setMessage(String  message) {
        this.message = message;
    }

    /**
     * 返回 当前页
     */
    public Integer getPage() {
        return page;
    }

    /**
     * 设置 当前页
     */
    public void setPage(Integer page) {
        this.page = page;
    }

    /**
     * 返回 分页大小
     */
    public Integer getPageSize() {
        return pageSize;
    }

    /**
     * 设置 分页大小
     */
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    /**
     * 返回 总记录数
     */
    public Integer getTotalCount() {
        return totalCount;
    }

    /**
     * 设置 总记录数
     */
    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    /**
     * 返回 返回数据
     */
    public T getData() {
        return data;
    }

    /**
     * 设置 返回数据
     */
    public void setData(T data) {
        this.data = data;
    }
}

