import {server} from '@/tools/servers'

/**
  * @author:${author}
  * @date:${date}
  */
export class ${className}Api {
    static AddApi() {
        return server.connection('POST', '${className}/add')
    }

    static DelApi(id) {
        return server.connection('POST', `${className}/del`)
    }

    static FindEntityByIdApi(data) {
        return server.connection('GET', `${className}/findEntityById`, data)
    }

    static UpdateApi(data) {
        return server.connection('POST', `${className}/update`, data)
    }
}