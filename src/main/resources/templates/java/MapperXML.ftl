<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="${mapperPackageName}.${className}Mapper">
    <resultMap id="${className}ResultMap" type="com.ddd.codegeneration.entity.entity.${className}">
        <id column="id" property="id" jdbcType="BIGINT" />
		<#list attrs as attr>
		<result column="${attr.name}" property="${attr.name}" />
        </#list>
    </resultMap>
    <sql id="Base_Column_List" >
        ${params}
    </sql>
    <insert id="insert" parameterType="com.ddd.codegeneration.entity.entity.${className}">
        <selectKey resultType="java.lang.Integer" keyProperty="id" order="AFTER" >
            SELECT LAST_INSERT_ID()
        </selectKey>
    ${insert}
    </insert>

    <delete id="deleteByPrimaryKey" parameterType="java.lang.Integer">
    ${deleteByPrimaryKey}
    </delete>

    <select id="selectAll" resultMap="${className}ResultMap">
    ${selectAll}
    </select>

    <select id="selectByPrimaryKey" resultMap="${className}ResultMap" parameterType="java.lang.Integer">
    ${selectByPrimaryKey}
    </select>

    <update id="updateByPrimaryKey" parameterType="com.ddd.codegeneration.entity.entity.${className}">
    ${updateByPrimaryKey}
    </update>

</mapper>
