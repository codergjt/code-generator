package ${serviceImplPackageName};

import com.ddd.codegeneration.entity.entity.${className};
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import ${mapperPackageName}.${className}Mapper;
import ${servicePackageName}.${className}Service;
/**
* 描述：${className}ServiceImpl 服务接口
* @author ${author}
* @date ${date}
*/
@Component
@Transactional
public class ${className}ServiceImpl implements ${className}Service{

        @Autowired
        private ${className}Mapper ${className?uncap_first}Mapper;
        /**
          *  根据ID查找${className}对象
          */
        @Override
        public ${className} findEntityById(Integer id) throws Exception {
            return this.${className?uncap_first}Mapper.selectByPrimaryKey(id);
        }

        /**
          *  新增一个${className}对象
          */
        @Override
        public Integer create${className}(${className} ${className?uncap_first}) throws Exception {
            return this.${className?uncap_first}Mapper.insert(${className?uncap_first});
        }

        /**
          *  根据ID删除一个${className}对象
          */
        @Override
        public Integer delete${className}(Integer id) throws Exception {
            return this.${className?uncap_first}Mapper.deleteByPrimaryKey(id);
        }
         /**
           *  根据ID更新
           */
        @Override
        public Integer update${className}(${className} ${className?uncap_first}) throws Exception {
            return this.${className?uncap_first}Mapper.updateByPrimaryKey(${className?uncap_first});
        }

}