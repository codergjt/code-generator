package ${entityPackageName};


<#if attrs?exists>
    <#list attrs as attr>
        <#if  (attr.type = 'List')>
import java.util.List;
        </#if>
        <#if  (attr.type = 'Map')>
import java.util.Map;
        </#if>
        <#if  (attr.type = 'HashMap')>
import java.util.HashMap;
        </#if>
        <#if  (attr.type = 'Date')>
import java.util.Date;
        </#if>
        <#if  (attr.type = 'ArrayList')>
import java.util.ArrayList;
        </#if>
    </#list>
</#if>
/**
 *  @描述：${className}实体
 *  @author ${author}
 *  @date ${date}
 */
public class ${className} {
<#if attrs?exists>
    <#list attrs as attr>
    private ${attr.type} ${attr.name};
    
    </#list>
</#if>

    public ${className}(){}

    public ${className}(${columns}){
<#list attrs as attr>
    this.${attr.name} = ${attr.name };
</#list>
    }

<#if attrs?exists>
    <#list attrs as attr>
    public void set${attr.name?cap_first}(${attr.type} ${attr.name}){
        this.${attr.name} = ${attr.name};
    }
    public ${attr.type} get${attr.name?cap_first}(){
        return this.${attr.name};
    }

    </#list>
</#if>
}