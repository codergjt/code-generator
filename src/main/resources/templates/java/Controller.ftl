package ${controllerPackageName};

import com.ddd.codegeneration.entity.entity.${className};
import com.ddd.codegeneration.service.${className}Service;
import com.ddd.codegeneration.serviceImpl.${className}ServiceImpl;
import com.ddd.codegeneration.utils.JSONResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import javax.validation.Valid;

/**
 * @author ${className}
 * @version 1.0
 * @Title:  代码生成接口，用于接受前端传输过来的数据
 * @date 2018/7/22/21:26
 */
@RequestMapping(value = "${className}Controller")
@CrossOrigin
@RestController
public class ${className}Controller {

    @Autowired
    private ${className}Service ${className?uncap_first}Service;

    /**
     * 新增
     * @param ${className?uncap_first}  前台传过来的实体参数
     * @return
     */
    @RequestMapping(method = RequestMethod.POST, value = "/add")
    public JSONResult addJavaCode(@Valid @RequestBody ${className} ${className?uncap_first}) {
        JSONResult jsonResult = new JSONResult<>();
        try {
            int result = ${className?uncap_first}Service.create${className}(${className?uncap_first});
            if(result!=0){
                jsonResult.setMessage("新增成功");
            } else {
                jsonResult.setMessage("新增失败");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return jsonResult;
    }

   /**
     * 删除
     * @param id  前台传过来的实体的ID
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/del")
    public JSONResult del(@Valid Integer id) {
        JSONResult jsonResult = new JSONResult<>();
        try {
            int result = ${className?uncap_first}Service.delete${className}(id);
            if(result!=0){
                jsonResult.setMessage("删除成功");
            } else {
                jsonResult.setMessage("删除失败");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return jsonResult;
    }

    /**
     * 根据Id找到对应的实体
     * @param id  前台传过来的实体的ID
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/findEntityById")
    public JSONResult findEntityById(@Valid  Integer id) {
        JSONResult jsonResult = new JSONResult<>();
        ${className}  ${className?uncap_first} = new ${className}();
        try {
            ${className?uncap_first} = ${className?uncap_first}Service.findEntityById(id);
            if(${className?uncap_first} != null){
                jsonResult.setMessage("找到结果");
                jsonResult.setData(${className?uncap_first});
            } else {
                jsonResult.setMessage("未找到");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return jsonResult;
    }

    /**
     * 根据ID更新
     * @param ${className?uncap_first}  前台传过来的实体参数
     * @return
     */
    @RequestMapping(method = RequestMethod.POST, value = "/update")
    public JSONResult add(@Valid @RequestBody ${className} ${className?uncap_first}) {
        JSONResult jsonResult = new JSONResult<>();
        try {
            int result = ${className?uncap_first}Service.update${className}(${className?uncap_first});
            if(result!=0){
                jsonResult.setMessage("更新成功");
            } else {
                jsonResult.setMessage("更新失败");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return jsonResult;
    }

}
