package ${servicePackageName};

import com.ddd.codegeneration.entity.entity.${className};
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
/**
* 描述：${className}Service 服务接口
* @author ${author}
* @date ${date}
*/
@Component
@Transactional
public interface ${className}Service {

    /**
      * 根据ID查找${className}对象
      * @author ${author}
      * @date ${date}
      */
    ${className} findEntityById(Integer id)throws Exception;
    /**
      * 新增一个${className}对象
      * @author ${author}
      * @date ${date}
      */
    Integer create${className}(${className} ${className?uncap_first}) throws Exception;
    /**
      * 根据ID删除一个${className}对象
      * @author ${author}
      * @date ${date}
      */
     Integer delete${className}(Integer id) throws Exception;
     /**
       * 根据ID更新
       * @author ${author}
       * @date ${date}
       */
    Integer update${className}(${className} ${className?uncap_first}) throws Exception;

}