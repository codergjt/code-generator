package ${mapperPackageName};

import java.util.List;

import com.ddd.codegeneration.entity.entity.${className};

public interface ${className}Mapper {

    int deleteByPrimaryKey(Integer id);

    int insert(${className} record);

    ${className} selectByPrimaryKey(Integer id);

    int updateByPrimaryKey(${className} record);

    List<${className}> selectAll();
}