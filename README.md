# 基于FreeMarker的代码生成插件



## 主要技术栈：

​	java

​	FreeMarker

​	springboot

​	mybatis	



## 后端：

> ​	主要实现springboot的4层代码生成，比如增删改查。

## 前端：

> ​	主要生成以vue为主的基础页面代码。



```
7/23 完成基础代码生成，已经可以生成除了XML以外的所有代码

7/24 完成所有后台代码生成操作。新增yml文件读取工具类。

7/25 完善代码结构，放弃前端代码生成(前端生成的代码是vue的，个人认为没有必要)。
```



## 使用方法：

​	将codegeneration目录下所有文件分别复制到对应的文件夹中；

​	在application中添加启动方法；启动既可以；

​	使用过之前请添加和修改文件templateConfig.yml：

​	

```yaml
generator:
  java:
    code:
      entityPath: E:\java-workspace\codegeneration\src\main\java\com\ddd\codegeneration\entity\entity\
      servicePath: E:\java-workspace\codegeneration\src\main\java\com\ddd\codegeneration\service\
      serviceImplPath: E:\java-workspace\codegeneration\src\main\java\com\ddd\codegeneration\serviceImpl\
      controllerPath: E:\java-workspace\codegeneration\src\main\java\com\ddd\codegeneration\controller\
      mapperPath: E:\java-workspace\codegeneration\src\main\java\com\ddd\codegeneration\dao\mapper\
      xmlPath: E:\java-workspace\codegeneration\src\main\resources\Mapper\
    package:
      entityPackage: com.ddd.codegeneration.entity.entity
      servicePackage:  com.ddd.codegeneration.service
      serviceImplPackage: com.ddd.codegeneration.serviceImpl
      controllerPackage:  com.ddd.codegeneration.controller
      mapperPackage:  com.ddd.codegeneration.dao.mapper
    author: test

```

​	

